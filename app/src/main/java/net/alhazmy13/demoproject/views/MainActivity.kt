package net.alhazmy13.demoproject.views

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import net.alhazmy13.demoproject.R
import net.alhazmy13.demoproject.model.PostModel
import android.arch.lifecycle.ViewModelProviders
import net.alhazmy13.demoproject.viewmodels.MainViewModel


class MainActivity : AppCompatActivity() {

    private var listView: ListView? = null
    private var adapter: SimpleArrayAdapter? = null
    private var postModels: MutableList<PostModel> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = findViewById(R.id.listView)
        adapter = SimpleArrayAdapter(postModels)
        listView?.adapter = adapter

        val mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        mainViewModel.getPostModels()?.observe(this, Observer { result ->
            postModels.clear()
            postModels.addAll(result!!.asIterable())
            adapter?.notifyDataSetChanged()

        })

    }


    inner class SimpleArrayAdapter(private val values: List<PostModel>?) :
        ArrayAdapter<PostModel>(this@MainActivity, -1, values) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val rowView = inflater.inflate(R.layout.item_post, parent, false)
            val body: TextView = rowView.findViewById(R.id.txt_body)
            val title: TextView = rowView.findViewById(R.id.txt_title)
            body.text = values?.get(position)?.getBody()
            title.text = values?.get(position)?.getTitle()

            return rowView
        }
    }
}
