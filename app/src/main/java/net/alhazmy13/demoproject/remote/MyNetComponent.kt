package net.alhazmy13.demoproject.remote

import net.alhazmy13.demoproject.viewmodels.MainViewModel

import javax.inject.Singleton

import dagger.Component

@Singleton
@Component(modules = [MyNetModule::class])
interface MyNetComponent {

    fun inject(target: MainViewModel)

}
