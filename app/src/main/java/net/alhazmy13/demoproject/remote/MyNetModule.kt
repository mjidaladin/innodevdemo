package net.alhazmy13.demoproject.remote

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


/**
 * Created by alaa on 27/09/2018 AD.
 */

@Module
class MyNetModule(private val mBaseUrl: String) {

    @Provides
    @Singleton
    fun providePostApi(retrofit: Retrofit): MyServiceApi {
        return retrofit.create(MyServiceApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(mBaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    }

}