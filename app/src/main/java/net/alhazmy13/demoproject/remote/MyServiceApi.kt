package net.alhazmy13.demoproject.remote

import net.alhazmy13.demoproject.model.PostModel
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by alaa on 27/09/2018 AD.
 */


interface MyServiceApi {

    @GET("/posts")
    fun getPostModels(): Call<List<PostModel>>

}