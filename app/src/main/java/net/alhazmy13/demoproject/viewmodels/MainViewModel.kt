package net.alhazmy13.demoproject.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log

import net.alhazmy13.demoproject.model.PostModel
import net.alhazmy13.demoproject.remote.MyServiceApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


/**
 * Created by alaa on 27/09/2018 AD.
 */

class MainViewModel : BaseViewModel() {

    @Inject
    lateinit var myServiceApi: MyServiceApi

    private var postModels: MutableLiveData<List<PostModel>>? = null

    fun getPostModels(): LiveData<List<PostModel>>? {
        if (this.postModels == null) {
            this.postModels = MutableLiveData()
            // get postModels asynchronous using Retrofit
            myServiceApi.getPostModels().enqueue(object : Callback<List<PostModel>> {
                override fun onResponse(
                    call: Call<List<PostModel>>,
                    response: Response<List<PostModel>>
                ) {

                    if (response.isSuccessful) {
                        this@MainViewModel.postModels?.value = response.body()
                        Log.d("MainViewModel", "posts loaded from API")
                    } else {
                        val statusCode = response.code()
                        Log.d("MainViewModel", "posts not loaded from API with error $statusCode")
                    }
                }

                override fun onFailure(call: Call<List<PostModel>>, t: Throwable) {
                    Log.d("MainViewModel", "error loading from API")

                }
            })
        }
        return this.postModels
    }

}
