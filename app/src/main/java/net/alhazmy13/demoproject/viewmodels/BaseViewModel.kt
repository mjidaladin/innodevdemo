package net.alhazmy13.demoproject.viewmodels

import android.arch.lifecycle.ViewModel
import net.alhazmy13.demoproject.remote.DaggerMyNetComponent
import net.alhazmy13.demoproject.remote.MyNetConstants
import net.alhazmy13.demoproject.remote.MyNetComponent
import net.alhazmy13.demoproject.remote.MyNetModule

/**
 * Created by alaa on 29/09/2018 AD.
 */

abstract class BaseViewModel : ViewModel() {

    // build component using Dagger for Dependency Injection
    private val myNetComponent: MyNetComponent = DaggerMyNetComponent.builder()
        .myNetModule(MyNetModule(MyNetConstants.BASE_URL))
        .build()

    init {
        injectDependencies()
    }

    private fun injectDependencies() {
        when (this) {
            is MainViewModel -> myNetComponent.inject(this)
        }
    }

}